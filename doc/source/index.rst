.. _baremetalpool:

=============================
BareMetalPool Custom Resource
=============================

.. toctree::
    :maxdepth: 2

    overview
    api
    parameters
