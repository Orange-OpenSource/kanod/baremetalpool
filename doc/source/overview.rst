.. _baremetalpool_overview:

Overview
========
The role of the baremetalpool operator is to allocate a pool of baremetalhost in the current cluster.
When a baremetalhost is created, the baremetalpool operateur requests a server from the Redfish Broker.

The response to this request contains the information for creating the baremetalhost : 

* BMC address : virtual address which will be handled by the CoreDNS server of the cluster 
  and redirected to the Redfish Broker.
  The schema authorized for the BMC address are : ``redfish`` or ``redfish-virtualmedia``
* BMC credentials : equals to the credential of the pool
* other information/field used for baremetalhost creation

	- path
	- labels
	- annotations
	- deviceName
	- disableCertificateVerification

The number of baremetalhost desired is defined in the CRD specification.
The baremetalpool tries to keep the number of baremetalhost equal to the replicas numer defined in its spec :  

* If the replicas (spec) is ``greater`` than the number of baremetalhost present in the cluster, 
  the BareMetalPool operator will retrieve from the Redfish Broker the informations to create a new baremetalhost.
* If the replicas (spec) is ``lower`` than the number of baremetalhost present in the cluster, 
  the BareMetalPool operator will delete unused baremetalhost

The credentials defined in the custom resource of a baremetalpool are the credentials
of the corresponding pool in the Redfish Broker.
