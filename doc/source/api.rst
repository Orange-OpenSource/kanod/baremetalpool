.. _baremetalpool_api:

BareMetalPool Resource
======================

.. jsonschema:: schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:
