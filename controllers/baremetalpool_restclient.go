/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strings"

	"github.com/go-logr/logr"
	broker "gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"

	corev1 "k8s.io/api/core/v1"
)

type BrokerdefReq struct {
	Log          logr.Logger
	BrokerClient *broker.ClientWithResponses
}

// List servers
func (b *BrokerdefReq) listServerRequest(
	poolSecret *corev1.Secret,
	brokerRedfishAddress string,
	poolId string,
) (*[]broker.ServerResponse, *broker.Error, error) {
	var ServerList []broker.ServerResponse
	var errorObject broker.Error

	resp, err := b.BrokerClient.GetAllServers(context.Background(),
		poolId,
		addBasicAuth(string(poolSecret.Data["username"]), string(poolSecret.Data["password"])))

	if err != nil {
		b.Log.Info(fmt.Sprintf("Failed to send request to broker (IP: %s)", brokerRedfishAddress))
		b.Log.Info(fmt.Sprint(err))
		return nil, nil, err
	}

	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	if resp.StatusCode == 200 {
		err = json.Unmarshal(bodyText, &ServerList)
		if err != nil {
			b.Log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return nil, nil, err
		}
		return &ServerList, nil, nil
	} else {
		err = json.Unmarshal(bodyText, &errorObject)
		if err != nil {
			b.Log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return nil, nil, err
		}
		return nil, &errorObject, nil
	}
}

// Delete server
func (b *BrokerdefReq) deleteServerRequest(
	poolSecret *corev1.Secret,
	brokerRedfishAddress string,
	poolId string,
	serverName string,
) (*ErrorResponse, error) {
	var errorObject ErrorResponse

	resp, err := b.BrokerClient.RemoveServer(context.Background(),
		poolId, serverName,
		addBasicAuth(string(poolSecret.Data["username"]), string(poolSecret.Data["password"])))

	if err != nil {
		return nil, err
	}

	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		b.Log.Info(fmt.Sprint(err))
		return nil, err
	}

	if resp.StatusCode == 200 {
		return nil, nil
	} else {
		err = json.Unmarshal(bodyText, &errorObject)
		if err != nil {
			b.Log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return nil, err
		}
		return &errorObject, nil
	}
}

// Create server
func (b *BrokerdefReq) createServerRequest(
	poolSecret *corev1.Secret,
	brokerRedfishAddress string,
	poolId string,
	serverName string,
	postData *broker.ServerRequest,
) (*broker.ServerResponse, *broker.Error, error) {
	var respObject broker.ServerResponse
	var errorObject broker.Error

	resp, err := b.BrokerClient.CreateServer(context.Background(),
		poolId, *postData,
		addBasicAuth(string(poolSecret.Data["username"]), string(poolSecret.Data["password"])))

	if err != nil {
		return nil, nil, err
	}

	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	if resp.StatusCode == 201 {
		err = json.Unmarshal(bodyText, &respObject)
		if err != nil {
			b.Log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return nil, nil, err
		}
		return &respObject, nil, nil
	} else {
		err = json.Unmarshal(bodyText, &errorObject)
		if err != nil {
			b.Log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return nil, nil, err
		}
		return nil, &errorObject, nil
	}

}

func (b *BrokerdefReq) checkPool(
	poolSecret *corev1.Secret,
	brokerRedfishAddress string,
	poolId string,
) bool {
	resp, _ := b.BrokerClient.GetPool(context.Background(),
		poolId,
		addBasicAuth(string(poolSecret.Data["username"]), string(poolSecret.Data["password"])))

	return resp.StatusCode == 200
}

func (b *BrokerdefReq) createPool(
	poolSecret *corev1.Secret,
	brokerRedfishAddress string,
	request *broker.PoolRequest,
) error {

	resp, err := b.BrokerClient.CreatePool(context.Background(),
		*request,
		addBasicAuth(string(poolSecret.Data["username"]), string(poolSecret.Data["password"])))

	if err != nil {
		b.Log.Info(fmt.Sprintf("Error during call to CreatePool: %s", fmt.Sprint(err)))
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("cannot create pool - %d", resp.StatusCode)
	}
	return nil
}

// delete server
func (b *BrokerdefReq) deleteServer(
	poolName string,
	bmhName string,
	poolSecret *corev1.Secret,
	redfishBrokerAddress string,
) error {
	splitStr := strings.Split(bmhName, "-")
	if len(splitStr) != 2 {
		err := errors.New("failed to get server name to delete")
		b.Log.Error(err, "Error during name splitting")
		return err
	}
	serverName := splitStr[1]

	b.Log.Info("Deleting server", "serverName : ", serverName)

	errorData, err := b.deleteServerRequest(poolSecret, redfishBrokerAddress, poolName, serverName)
	if err != nil {
		b.Log.Info(fmt.Sprintf("Error during server deletion : %s", serverName))
		return err
	}

	if errorData != nil {
		err = errors.New(errorData.Message)
		b.Log.Info(fmt.Sprintf("Failed to delete the server %s", serverName))
		return err
	}

	return nil
}
