/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"reflect"
	"sort"
	"time"

	"strings"

	"github.com/go-logr/logr"
	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	bmpv1 "gitlab.com/Orange-OpenSource/kanod/baremetalpool/api/v1"
	broker "gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/rand"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// BareMetalPoolReconciler reconciles a BareMetalPool object
type BareMetalPoolReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type ErrorResponse struct {
	Message string `json:"message"`
}

// MilliResyncPeriod is the duration between two synchronization attempt of a given resource
const (
	MilliResyncPeriod   = 5000
	BMH_BYOH_ANNOTATION = "kanod.io/bmh-used-by-byohmachine"
)

//
//+kubebuilder:rbac:groups=bmp.kanod.io,resources=baremetalpools,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=bmp.kanod.io,resources=baremetalpools/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=bmp.kanod.io,resources=baremetalpools/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;delete
//+kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;delete
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/status,verbs=get;update;patch

func NewHttpClient(Certificate string) *http.Client {
	pool := x509.NewCertPool()
	if Certificate != "" {
		pool.AppendCertsFromPEM([]byte(Certificate))
	}
	tlsConfig := &tls.Config{
		RootCAs: pool,
	}
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	return &http.Client{Transport: transport}
}

func WithHttpsClient(Certificate string) broker.ClientOption {
	return func(c *broker.Client) error {

		c.Client = NewHttpClient(Certificate)
		return nil
	}
}

func addBasicAuth(username string, password string) broker.RequestEditorFn {
	return func(ctx context.Context, req *http.Request) error {
		req.SetBasicAuth(string(username), string(password))
		return nil
	}
}

func (r *BareMetalPoolReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var statusMessage string = ""
	var poolSize int32

	log := r.Log.WithValues("baremetalpool", req.NamespacedName)

	// Fetch the BareMetalPool instance
	baremetalpool := &bmpv1.BareMetalPool{}
	err := r.Get(ctx, req.NamespacedName, baremetalpool)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			log.Info("BareMetalPool resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Error(err, "Failed to get BareMetalPool")
		return ctrl.Result{}, err
	}

	annotations := baremetalpool.GetAnnotations()
	if annotations != nil {
		if _, ok := annotations[bmpv1.PausedAnnotation]; ok {
			log.Info("baremetalpool is paused")
			return ctrl.Result{Requeue: false}, nil
		}
	}

	// Fetch pool secret
	redfishBrokerAddress := baremetalpool.Spec.Address
	poolSecret := &corev1.Secret{}
	key := client.ObjectKey{
		Namespace: baremetalpool.Namespace,
		Name:      baremetalpool.Spec.CredentialName,
	}
	if err := r.Get(ctx, key, poolSecret); err != nil {
		log.Info("unable to get baremetalpool secret yet")
	}

	//Initialize http client
	var config = &corev1.ConfigMap{}
	cmConfig := client.ObjectKey{
		Name:      baremetalpool.Spec.BrokerConfig,
		Namespace: baremetalpool.Namespace,
	}
	err = r.Client.Get(ctx, cmConfig, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the configmap", "configmapName", baremetalpool.Spec.BrokerConfig)
		return ctrl.Result{}, err
	}
	brokerCA, ok := config.Data["broker_ca"]
	if !ok {
		r.Log.Error(err, "Missing broker_ca data in configmap", "configmapName", baremetalpool.Spec.BrokerConfig)
		return ctrl.Result{}, err
	}

	var brokerRequester BrokerdefReq
	brokerRequester.Log = ctrl.Log.WithName("Broker")
	brokerRequester.BrokerClient, err = broker.NewClientWithResponses(redfishBrokerAddress, WithHttpsClient(string(brokerCA)))
	if err != nil {
		r.Log.Error(err, "can not get brokerdef http client")
		return ctrl.Result{}, err
	}

	poolName := baremetalpool.Spec.PoolName

	// check availability,if none create baremetalPool and resync.
	if !brokerRequester.checkPool(poolSecret, redfishBrokerAddress, poolName) {
		log.Info("Create Pooldef on distant server.")
		poolRequest := broker.PoolRequest{
			Id:            &poolName,
			LabelSelector: &baremetalpool.Spec.LabelSelector,
			MaxServers:    &baremetalpool.Spec.MaxServers,
		}
		err = brokerRequester.createPool(poolSecret, redfishBrokerAddress, &poolRequest)
		if err != nil {
			log.Error(err, "Cannot create pooldef.")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
	}

	if annotations != nil {
		if _, ok := annotations[bmpv1.BmpPausedAnnotation]; ok {
			log.Info("baremetalpool is disabled")
			return ctrl.Result{Requeue: false}, nil
		}
	}

	// List attached baremetalhost locally
	lbls := labels.Set{
		bmpv1.KanodBareMetalPoolNameLabel: baremetalpool.Name,
	}
	bmhList := &bmhv1alpha1.BareMetalHostList{}
	if err = r.List(ctx, bmhList, &client.ListOptions{
		Namespace:     baremetalpool.Namespace,
		LabelSelector: labels.SelectorFromSet(lbls),
	}); err != nil {
		log.Error(err, "Failed to list bmh", "BareMetalPool.Namespace", baremetalpool.Namespace, "BareMetalPool.Name", baremetalpool.Name)
		return ctrl.Result{}, err
	}

	serverListData, errorData, err := brokerRequester.listServerRequest(poolSecret, redfishBrokerAddress, poolName)
	if err != nil {
		log.Info("error when listing servers on redfish broker")
		return ctrl.Result{}, err
	}

	if errorData != nil {
		log.Info("Failed to list servers on redfish broker")
		return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
	}

	mapBmhWithServer := make(map[string]bool)
	for _, serv := range *serverListData {
		bmhAssociated := fmt.Sprintf("%s-%s", poolName, serv.Id)
		mapBmhWithServer[bmhAssociated] = true
	}

	if !baremetalpool.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.finalizeBareMetalPoolDeletion(&ctx, brokerRequester, baremetalpool, bmhList, poolSecret, redfishBrokerAddress, log, mapBmhWithServer)
	}

	// add finalizer
	if !controllerutil.ContainsFinalizer(baremetalpool, bmpv1.BareMetalPoolFinalizer) {
		key = client.ObjectKey{
			Name:      baremetalpool.Name,
			Namespace: baremetalpool.Namespace,
		}

		baremetalpool = &bmpv1.BareMetalPool{}
		err = retry.RetryOnConflict(retry.DefaultRetry,
			func() error {
				if err := r.Get(ctx, key, baremetalpool); err != nil {
					return err
				}

				controllerutil.AddFinalizer(baremetalpool, bmpv1.BareMetalPoolFinalizer)
				err = r.Update(ctx, baremetalpool)
				return err
			})

		if err != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{Requeue: true}, nil
	}

	// Get baremetalhost names list
	sort.SliceStable(bmhList.Items, func(i, j int) bool { return bmhList.Items[i].Name < bmhList.Items[j].Name })
	bmhNames := getBmhNames(bmhList.Items)

	// count baremetalhost not in deletion process
	bmhCount := int32(0)
	for _, bmh := range bmhList.Items {
		if bmh.GetObjectMeta().GetDeletionTimestamp() == nil {
			bmhCount += 1
		}
	}

	// delete servers when bmh contains only the baremetalpool finalizer
	for _, bmh := range bmhList.Items {
		if bmh.GetObjectMeta().GetDeletionTimestamp() != nil {
			if len(bmh.GetFinalizers()) == 1 && (bmh.GetFinalizers())[0] == bmpv1.BareMetalHostFinalizer {
				err = r.finalizeServerDeletion(&ctx, brokerRequester, baremetalpool, bmh, poolSecret, redfishBrokerAddress, log, mapBmhWithServer)
				if err != nil {
					log.Info("Failed to finalize server deletion ")
					return ctrl.Result{}, err
				}
				return ctrl.Result{Requeue: true}, nil
			}
		}
	}

	// Delete servers (on redfish-broker) not associated with Baremletalhost
	err = r.deleteUnusedServer(brokerRequester, baremetalpool, bmhList.Items, poolSecret, redfishBrokerAddress, *serverListData)
	if err != nil {
		log.Info("Failed to delete unused server ")
		return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
	}

	// Delete baremetalhost not associated with one server
	bmhDeletetion, err := r.deleteOrphanBareMetals(&ctx,
		baremetalpool, bmhList.Items, poolSecret, redfishBrokerAddress, *serverListData)
	if err != nil {
		log.Info("Failed to delete orphan baremetal ")
		return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
	}

	if bmhDeletetion {
		return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
	}

	// Update bmh annotation with values from baremetaldef stored in broker
	for _, bmh := range bmhList.Items {
		annotationFromBroker := r.getK8SAnnotationForBmh(&ctx, baremetalpool, bmh.Name, log, *serverListData)
		err := r.UpdateBmhAnnotations(&ctx, baremetalpool, bmh.Name, annotationFromBroker, log)
		if err != nil {
			log.Info("Failed to update baremetalhost annotation", "bmh.Name", bmh.Name, "annotations", annotationFromBroker)
			return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
		}
	}

	if baremetalpool.Spec.Replicas != nil {
		poolSize = *(baremetalpool.Spec.Replicas)

	}

	// Ensure the baremetalhost count is the same as the size in the baremetalpool spec
	if bmhCount == poolSize {
		statusMessage = "Pool allocated"
	}

	// baremetalhost count is smaller than the size in the baremetalpool spec
	if bmhCount < poolSize {
		// Create bmh - send request to proxy server
		statusMessage = "Under-allocation"

		serverName := string(rand.String(10))
		postData := broker.ServerRequest{
			Id:     serverName,
			Schema: baremetalpool.Spec.RedfishSchema,
		}
		log.Info("Creating a new server", "serverName", serverName)
		respData, errorData, err := brokerRequester.createServerRequest(poolSecret, redfishBrokerAddress, poolName, serverName, &postData)
		if err != nil {
			log.Info("Failed to get a server for creating a new baremetalhost")
			return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
		}

		if errorData != nil {
			log.Info("Error during server creation : ", "errorData.Message", errorData.Message)
			statusMessage = fmt.Sprintf("Under-allocation : %s", errorData.Message)
		} else {
			bmhName := fmt.Sprintf("%s-%s", baremetalpool.Spec.PoolName, serverName)

			bmh := r.bmhForBareMetalPool(baremetalpool, bmhName, *respData, log)
			log.Info("Creating a new baremetalhost", "bmh.Namespace", bmh.Namespace, "bmh.Name", bmh.Name, "annotations", bmh.GetAnnotations())
			err = r.Create(ctx, bmh)
			if err != nil {
				log.Error(err, "Failed to create new bmh", "bmh.Namespace", bmh.Namespace, "bmh.Name", bmh.Name)
				return ctrl.Result{}, err
			}

			bmhSecret := r.createBmhSecret(baremetalpool, bmh.Name, poolSecret)
			if err := ctrl.SetControllerReference(bmh, bmhSecret, r.Scheme); err != nil {
				log.Error(err, "Failed to set controller reference on secret")
				return ctrl.Result{}, err
			}
			err = r.Create(ctx, bmhSecret)
			if err != nil {
				log.Error(err, "Failed to create new bmh secret", "bmhSecret", bmhSecret)
				return ctrl.Result{}, err
			}
		}
	}

	// active baremetalhost count is greater than the size in the baremetalpool spec
	if bmhCount > poolSize {
		// Delete bmh
		statusMessage = "Over-allocation"

		// delete only one bmh, not already in deleting state
		for _, bmh := range bmhList.Items {
			if !isBmhValidForDelete(bmh) {
				continue
			}
			if bmh.GetObjectMeta().GetDeletionTimestamp() != nil {
				continue
			}
			err := r.deleteBareMetalHost(&ctx, bmh, *baremetalpool)
			if err != nil {
				log.Info("failed to delete a baremetalhost", "baremetalhost", bmh.Name)
				return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
			}

			break
		}
	}

	err = r.updateStatus(req.NamespacedName, bmhNames, bmhCount, statusMessage)
	if err != nil {
		log.Error(err, "Failed to update status ")
		return ctrl.Result{}, err
	}

	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

// delete unused server for the pool
func (r *BareMetalPoolReconciler) deleteUnusedServer(
	brokerRequester BrokerdefReq,
	m *bmpv1.BareMetalPool,
	bmhList []bmhv1alpha1.BareMetalHost,
	poolSecret *corev1.Secret,
	redfishBrokerAddress string,
	serverListData []broker.ServerResponse,
) error {
	log := r.Log.WithValues()
	bmhNamesList := make(map[string]bool)

	for _, bmh := range bmhList {
		bmhName := bmh.GetObjectMeta().GetName()
		bmhNamesList[bmhName] = true
	}

	for _, serv := range serverListData {
		bmhName := fmt.Sprintf("%s-%s", m.Spec.PoolName, serv.Id)
		_, found := bmhNamesList[bmhName]
		if !found {
			log.Info("Delete unused server on redfish broker: ", "serv.Id", serv.Id)
			err := brokerRequester.deleteServer(m.Spec.PoolName, bmhName, poolSecret, redfishBrokerAddress)

			if err != nil {
				log.Info("Error during unused server deletion")
				return err
			}
		}
	}
	return nil
}

// Update the status of the baremetalpool
func (r *BareMetalPoolReconciler) updateStatus(
	key types.NamespacedName,
	bmhNames []string,
	bmhCount int32,
	message string,
) error {
	ctx := context.Background()
	log := r.Log.WithValues()

	status := bmpv1.BareMetalPoolStatus{
		Replicas:     bmhCount,
		BmhNamesList: bmhNames,
		Status:       message,
	}

	baremetalpool := &bmpv1.BareMetalPool{}

	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := r.Get(ctx, key, baremetalpool); err != nil {
				return err
			}

			if !reflect.DeepEqual(baremetalpool.Status, status) {
				baremetalpool.Status = status
				return r.Status().Update(ctx, baremetalpool)
			}

			return nil
		})

	if err != nil {
		log.Error(err, "failed to update baremetalpoolStatus")
	}

	return err
}

// Create the secret for the baremetalhost
func (r *BareMetalPoolReconciler) createBmhSecret(
	m *bmpv1.BareMetalPool,
	bmhName string,
	poolSecret *corev1.Secret,
) *corev1.Secret {

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-secret", bmhName),
			Namespace: m.Namespace,
		},
		Data: poolSecret.Data,
	}
	return secret
}

// Create the baremetalhost
func (r *BareMetalPoolReconciler) bmhForBareMetalPool(
	bmp *bmpv1.BareMetalPool,
	bmhName string,
	serverData broker.ServerResponse,
	log logr.Logger,
) *bmhv1alpha1.BareMetalHost {

	labels := map[string]string{
		bmpv1.KanodPoolNameLabel:          bmp.Spec.PoolName,
		bmpv1.KanodBareMetalPoolNameLabel: bmp.Name,
	}

	annotations := map[string]string{}

	if serverData.K8sLabels != nil {
		for key := range *serverData.K8sLabels {
			labels[key] = (*serverData.K8sLabels)[key]
		}
	}
	if serverData.K8sAnnotations != nil {
		for key := range *serverData.K8sAnnotations {
			annotations[key] = (*serverData.K8sAnnotations)[key]
		}
	}
	if bmp.Spec.BmhLabels != nil {
		for key := range bmp.Spec.BmhLabels {
			labels[key] = bmp.Spec.BmhLabels[key]
		}
	}

	if bmp.Spec.BmhAnnotations != nil {
		for key := range bmp.Spec.BmhAnnotations {
			annotations[key] = bmp.Spec.BmhAnnotations[key]
		}
	}

	if bmp.Spec.BlockingAnnotations != nil && len(bmp.Spec.BlockingAnnotations) != 0 {
		log.Info("add baremetalhost paused annotation", "baremetalhost", bmhName)

		annotations[bmhv1alpha1.PausedAnnotation] = bmpv1.BmhPausedKanodValue
	}

	disableCertificateVerification := false
	if serverData.DisableCertificateVerification != nil {
		disableCertificateVerification = *serverData.DisableCertificateVerification
	}

	bmhCred := fmt.Sprintf("%s-secret", bmhName)
	bmh := &bmhv1alpha1.BareMetalHost{
		ObjectMeta: metav1.ObjectMeta{Name: bmhName, Namespace: bmp.Namespace, Labels: labels, Annotations: annotations},
		Spec: bmhv1alpha1.BareMetalHostSpec{
			Online:         bmp.Spec.Online,
			BootMACAddress: *serverData.MacAddress,
			BootMode:       bmp.Spec.BootMode,
			BMC: bmhv1alpha1.BMCDetails{
				Address:                        *serverData.Url, // fmt.Sprintf("%s://%s.kanod.io%s", m.Spec.RedfishSchema, bmhName, serverData.Path),
				CredentialsName:                bmhCred,
				DisableCertificateVerification: disableCertificateVerification,
			},
		},
		Status: bmhv1alpha1.BareMetalHostStatus{},
	}
	if serverData.RootDeviceHints != nil {
		bmh.Spec.RootDeviceHints = (*serverData.RootDeviceHints).DeepCopy()
	}

	if bmp.Spec.Image != nil {
		bmh.Spec.Image = bmp.Spec.Image
	}
	if bmp.Spec.UserData != nil {
		bmh.Spec.UserData = bmp.Spec.UserData
	}
	if bmp.Spec.PreprovisioningNetworkDataName != "" {
		bmh.Spec.PreprovisioningNetworkDataName = bmp.Spec.PreprovisioningNetworkDataName
	}
	if bmp.Spec.NetworkData != nil {
		bmh.Spec.NetworkData = bmp.Spec.NetworkData
	}
	if bmp.Spec.MetaData != nil {
		bmh.Spec.MetaData = bmp.Spec.MetaData
	}

	bmh.SetFinalizers(append(bmh.GetFinalizers(), bmpv1.BareMetalHostFinalizer))
	//  Only one reference can have Controller set to true, baremetal-operator will control the bmh
	//	ctrl.SetControllerReference(m, bmh, r.Scheme)
	return bmh
}

// getBmhNames
func getBmhNames(bmhs []bmhv1alpha1.BareMetalHost) []string {
	var bmhNames []string
	for _, bmh := range bmhs {
		bmhNames = append(bmhNames, bmh.Name)
	}
	return bmhNames
}

// SetupWithManager sets up the controller with the Manager.
func (r *BareMetalPoolReconciler) SetupWithManager(mgr ctrl.Manager) error {
	c := ctrl.NewControllerManagedBy(mgr)
	return c.For(&bmpv1.BareMetalPool{}).Complete(r)
}

func isBmhValidForDelete(bmh bmhv1alpha1.BareMetalHost) bool {
	if (bmh.Status.Provisioning.State == bmhv1alpha1.StateReady) ||
		(bmh.Status.Provisioning.State == bmhv1alpha1.StateAvailable) {
		return true
	}
	if bmh.Status.Provisioning.State == bmhv1alpha1.StateProvisioned {
		annotations := bmh.GetAnnotations()

		if val, ok := annotations[BMH_BYOH_ANNOTATION]; ok {
			if val == "false" {
				return true
			}
		}
	}
	return false

}

// //////////////////////////////////////////////////////////////
func (r *BareMetalPoolReconciler) finalizeBareMetalPoolDeletion(
	ctx *context.Context,
	brokerRequester BrokerdefReq,
	baremetalpool *bmpv1.BareMetalPool,
	bmhList *bmhv1alpha1.BareMetalHostList,
	poolSecret *corev1.Secret,
	redfishBrokerAddress string,
	log logr.Logger,
	serverList map[string]bool,
) (ctrl.Result, error) {

	if controllerutil.ContainsFinalizer(baremetalpool, bmpv1.BareMetalPoolFinalizer) {
		log.Info("cascaded delete of baremetalhost ", "BareMetalPool", baremetalpool.Name)
		log.Info("finalizeBareMetalPoolDeletion : bmh list ", "len(bmhList.Items)", len(bmhList.Items))
		log.Info("finalizeBareMetalPoolDeletion : bmh list ", "bmhList.Items", bmhList.Items)
		for _, bmh := range bmhList.Items {
			if bmh.GetObjectMeta().GetDeletionTimestamp() == nil {
				log.Info("finalizeBareMetalPoolDeletion : deleting baremetalhost", "bmh.Name", bmh.Name)
				err := r.deleteBareMetalHost(ctx, bmh, *baremetalpool)
				if err != nil {
					log.Info("failed to delete a baremetalhost")
					return ctrl.Result{}, err
				}

			} else {
				if len(bmh.GetFinalizers()) == 1 && (bmh.GetFinalizers())[0] == bmpv1.BareMetalHostFinalizer {
					err := r.finalizeServerDeletion(ctx, brokerRequester, baremetalpool, bmh, poolSecret, redfishBrokerAddress, log, serverList)
					if err != nil {
						return ctrl.Result{}, err
					}
				}
			}
		}

		if len(bmhList.Items) != 0 {
			log.Info("finalizeBareMetalPoolDeletion : bmh list not empty, requeueing")
			return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
		}

		log.Info("finalizeBareMetalPoolDeletion : removing baremetalpool finalizer")
		controllerutil.RemoveFinalizer(baremetalpool, bmpv1.BareMetalPoolFinalizer)
		err := r.Update(*ctx, baremetalpool)
		if err != nil {
			return ctrl.Result{}, err
		}
	} else {
		log.Info("finalizeBareMetalPoolDeletion : simple deletetion ", "BareMetalPool", baremetalpool.Name)
	}

	err := r.deleteCredentials(ctx, baremetalpool, log)
	if err != nil {
		log.Info("failed to delete a secret")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// deleteCredentials
func (r *BareMetalPoolReconciler) deleteCredentials(
	ctx *context.Context,
	baremetalpool *bmpv1.BareMetalPool,
	log logr.Logger,
) error {
	secret := &corev1.Secret{}
	secretName := baremetalpool.Spec.CredentialName
	secretNamespace := baremetalpool.ObjectMeta.Namespace
	secretMeta := client.ObjectKey{Name: secretName, Namespace: secretNamespace}
	log.V(0).Info("Trying to delete secret associated with the baremetalpool", "baremetalpool", baremetalpool.Name, "secret", secretName)
	if err := r.Client.Get(*ctx, secretMeta, secret); err == nil {
		if err := r.Client.Delete(*ctx, secret); err != nil {
			log.Error(err, "Cannot delete secret", "secret", secretName)
			return err
		}
	} else if !k8serrors.IsNotFound(err) {
		log.Error(err, "Cannot access secret", "secret", secretName)
		return err
	}
	return nil
}

// delete baremetalhost
func (r *BareMetalPoolReconciler) deleteBareMetalHost(
	ctx *context.Context,
	bmhToDelete bmhv1alpha1.BareMetalHost,
	bareMetalPool bmpv1.BareMetalPool,
) error {
	log := r.Log.WithValues()

	// get last version version of bmh object
	bmh := &bmhv1alpha1.BareMetalHost{}

	key := client.ObjectKey{
		Name:      bmhToDelete.Name,
		Namespace: bmhToDelete.Namespace,
	}

	if err := r.Get(*ctx, key, bmh); err != nil {
		return err
	}

	if bmh.Spec.ConsumerRef != nil {
		if consumerRefMatches(bmh.Spec.ConsumerRef, bareMetalPool) {
			log.Info("Deleting bmh", "BareMetalHost : ", bmh.Name)
			err := r.Delete(*ctx, bmh)
			if err != nil {
				return err
			}
		} else {
			errorMsg := fmt.Sprintf("can not delete baremetalhost %s : consumerRef not assigned to the associated baremetalpool %s", bmh.Name, bareMetalPool.Name)
			log.Info(errorMsg)
			return fmt.Errorf(errorMsg)
		}
	} else {
		bmh.Spec.ConsumerRef = &corev1.ObjectReference{
			Kind:       bareMetalPool.Kind,
			Name:       bareMetalPool.Name,
			Namespace:  bareMetalPool.Namespace,
			APIVersion: bareMetalPool.APIVersion,
		}

		log.Info("Updating ConsumerRef on bmh before deletion", "BareMetalHost : ", bmh.Name)
		err := r.Update(*ctx, bmh)
		if err != nil {
			log.Info("failed to update consumerRef field for baremetalhost")
			return err
		}
	}
	return nil
}

// delete baremetalhost without server associated
func (r *BareMetalPoolReconciler) deleteOrphanBareMetals(
	ctx *context.Context,
	m *bmpv1.BareMetalPool,
	bmhList []bmhv1alpha1.BareMetalHost,
	poolSecret *corev1.Secret,
	redfishBrokerAddress string,
	serverListData []broker.ServerResponse,
) (bool, error) {
	var deletionOnGoing bool
	log := r.Log.WithValues()
	serverList := make(map[string]bool)
	deletionOnGoing = false

	for _, serv := range serverListData {
		serverList[serv.Id] = true
	}

	for _, bmh := range bmhList {
		if bmh.ObjectMeta.DeletionTimestamp.IsZero() {
			bmhName := bmh.GetObjectMeta().GetName()
			splitStr := strings.Split(bmhName, "-")
			if len(splitStr) != 2 {
				err := fmt.Errorf("failed to get server name for baremetalhost %s", bmhName)
				log.Error(err, "Error during name splitting")
				return deletionOnGoing, err
			}
			serverName := splitStr[1]

			_, found := serverList[serverName]
			if !found {
				if controllerutil.ContainsFinalizer(&bmh, bmpv1.BareMetalHostFinalizer) {
					controllerutil.RemoveFinalizer(&bmh, bmpv1.BareMetalHostFinalizer)
					err := r.Update(*ctx, &bmh)
					if err != nil {
						log.Info("Failed to update baremetalhost finalizer ")
						return deletionOnGoing, err
					}
				} else {
					log.Info("Deleting orphan bmh", "BareMetalHost : ", bmh.Name)
					err := r.deleteBareMetalHost(ctx, bmh, *m)
					if err != nil {
						log.Error(err, "failed to delete a baremetalhost")
						return deletionOnGoing, err
					}
					deletionOnGoing = true
				}
			}
		}
	}
	return deletionOnGoing, nil
}

// getK8SAnnotationForBmh
func (r *BareMetalPoolReconciler) getK8SAnnotationForBmh(
	ctx *context.Context,
	baremetalpool *bmpv1.BareMetalPool,
	bmhName string,
	log logr.Logger,
	serverListData []broker.ServerResponse,
) map[string]string {
	for _, server := range serverListData {
		name := fmt.Sprintf("%s-%s", baremetalpool.Spec.PoolName, server.Id)
		if name == bmhName {
			annotations := map[string]string{}
			if server.K8sAnnotations != nil {
				for key := range *server.K8sAnnotations {
					annotations[key] = (*server.K8sAnnotations)[key]
				}
			}
			return annotations
		}
	}
	return nil
}

// UpdateBmhAnnotations
func (r *BareMetalPoolReconciler) UpdateBmhAnnotations(
	ctx *context.Context,
	baremetalpool *bmpv1.BareMetalPool,
	bmhName string,
	newAnnotations map[string]string,
	log logr.Logger,
) error {
	key := client.ObjectKey{
		Name:      bmhName,
		Namespace: baremetalpool.Namespace,
	}

	bmh := &bmhv1alpha1.BareMetalHost{}

	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			var deleteBmhPausedAnnotation bool = false
			if err := r.Get(*ctx, key, bmh); err != nil {
				return err
			}
			initialBmhAnnotations := bmh.GetAnnotations()

			updatedBmhAnnotations := make(map[string]string)
			for key, value := range initialBmhAnnotations {
				updatedBmhAnnotations[key] = value
			}

			for key, value := range newAnnotations {
				updatedBmhAnnotations[key] = value
			}

			if baremetalpool.Spec.BlockingAnnotations == nil {
				deleteBmhPausedAnnotation = true
			} else {

				blockingAnnotationMissing := false
				for _, blockingAnnotation := range baremetalpool.Spec.BlockingAnnotations {
					if _, ok := bmh.GetAnnotations()[blockingAnnotation]; !ok {
						blockingAnnotationMissing = true
						log.Info("blocking annotation missing in baremetalpool", "BlockingAnnotations", blockingAnnotation, "baremetalpool.Spec.BlockingAnnotations", baremetalpool.Spec.BlockingAnnotations)
					}
				}
				if !blockingAnnotationMissing {
					deleteBmhPausedAnnotation = true
				}
			}

			if val, ok := updatedBmhAnnotations[bmhv1alpha1.PausedAnnotation]; ok {
				if (val == bmpv1.BmhPausedKanodValue) && deleteBmhPausedAnnotation {
					delete(updatedBmhAnnotations, bmhv1alpha1.PausedAnnotation)
					log.Info("remove baremetalhost paused annotation", "baremetalhost", bmh.Name)
				}
			}

			if !mapEqual(updatedBmhAnnotations, initialBmhAnnotations) {
				bmh.SetAnnotations(updatedBmhAnnotations)
				log.Info("Updating baremetalhost annotations", "annotation", updatedBmhAnnotations)
				return r.Update(*ctx, bmh)
			}
			return nil
		})
	if err != nil {
		log.Error(err, "Cannot update baremetalhost.")
	}
	return err
}

// //////////////////////////////////////////////////////////////
func (r *BareMetalPoolReconciler) finalizeServerDeletion(
	ctx *context.Context,
	brokerRequester BrokerdefReq,
	baremetalpool *bmpv1.BareMetalPool,
	bmh bmhv1alpha1.BareMetalHost,
	poolSecret *corev1.Secret,
	redfishBrokerAddress string,
	log logr.Logger,
	serverList map[string]bool,
) error {
	var err error

	if _, ok := serverList[bmh.Name]; ok {
		log.Info("finalizeServerDeletion : deleting server associated with baremetalhost", "bmh.Name", bmh.Name)
		err := brokerRequester.deleteServer(baremetalpool.Spec.PoolName, bmh.Name, poolSecret, redfishBrokerAddress)
		if err != nil {
			log.Info("Failed to delete server ")
			return err
		}
	}

	log.Info("finalizeServerDeletion : removing finalizer on baremetalhost", "bmh.Name", bmh.Name)

	key := client.ObjectKey{
		Name:      bmh.Name,
		Namespace: bmh.Namespace,
	}

	bmh2 := &bmhv1alpha1.BareMetalHost{}

	err = r.Get(*ctx, key, bmh2)
	if err == nil {
		controllerutil.RemoveFinalizer(bmh2, bmpv1.BareMetalHostFinalizer)
		err2 := r.Update(*ctx, bmh2)
		if err2 != nil {
			log.Info("Failed to remove baremetalhost finalizer ")
			return err2
		}
	} else {
		if !k8serrors.IsNotFound(err) {
			log.Info("Failed to remove baremetalhost finalizer")
			return err
		}
	}

	return nil
}

// mapEqual compares two maps.
func mapEqual(m1 map[string]string, m2 map[string]string) bool {
	if len(m1) != len(m2) {
		return false
	}
	for k1, e1 := range m1 {
		e2, ok := m2[k1]
		if !ok || e2 != e1 {
			return false
		}
	}
	return true
}

// check if baremetalhost consumerRef match the corresponding baremetalpool
func consumerRefMatches(consumer *corev1.ObjectReference, bmp bmpv1.BareMetalPool) bool {
	if consumer.Name != bmp.Name {
		return false
	}
	if consumer.Namespace != bmp.Namespace {
		return false
	}
	if consumer.Kind != bmp.Kind {
		return false
	}
	if consumer.APIVersion != bmp.APIVersion {
		return false
	}
	return true
}
