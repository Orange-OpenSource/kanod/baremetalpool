#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

url="$REPO_URL"
version="${BMP_VERSION}"
KUSTOMIZE_VERSION=${KUSTOMIZE_VERSION:-3.8.2}

groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')
artifactId="baremetalpool"

echo "Checking : ${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.yaml"
if ! curl --head --silent --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.yaml"; then

  echo "Uploading baremetalpool manifest to ${url}"

  # shellcheck disable=SC2086
  mvn ${MAVEN_CLI_OPTS} deploy:deploy-file ${MAVEN_OPTS} -DgroupId="${groupId}" -DartifactId="${artifactId}" \
    -Dversion="${version}" -Dtype=yaml -Dfile=baremetalpool.yml \
    -DrepositoryId=kanod -Durl="${url}"
else
  echo "baremetalpool manifest already exists on Nexus"
fi
