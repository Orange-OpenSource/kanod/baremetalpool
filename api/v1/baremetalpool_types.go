/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// baremetalpoolFinalizer is the name of the finalizer
	BareMetalPoolFinalizer string = "baremetalpool.kanod.io/finalizer"

	// PausedAnnotation is the annotation that pauses the reconciliation
	PausedAnnotation string = "baremetalpool.kanod.io/paused"

	// BmhPausedKanodValue is the kanod value got BmhPausedAnnotation annotation
	BmhPausedKanodValue string = "kanod.io/bmpool"

	// finalizer added by baremetalpool operator to the baremetalhost
	BareMetalHostFinalizer string = "kanod.io.bmh.finalizer"

	// KanodPoolNameLabel is the label containing the poolName value
	KanodPoolNameLabel = "kanod.io/poolname"

	// KanodBareMetalPoolnameLabel is the label containing the baremetalpool name value
	KanodBareMetalPoolNameLabel = "kanod.io/baremetalpool"

	// BmpPausedAnnotation is the annotation indicating that the baremetalpool is disabled
	BmpPausedAnnotation = "baremetalpool.kanod.io/disabled"
)

// BareMetalPoolSpec defines the desired state of BareMetalPool
type BareMetalPoolSpec struct {
	// Number of baremetals requested
	Replicas *int32 `json:"replicas,omitempty"`
	// Name of the pool delivering baremetal access
	PoolName string `json:"poolName"`
	// Name of the secret associated with the pool
	CredentialName string `json:"credentialName"`
	// Address of the redfish broker
	Address string `json:"address"`
	// label for the selection of the baremetal which will be associated with the pool
	LabelSelector map[string]string `json:"labelSelector,omitempty"`
	// Number max of baremetal which can be associated with the pool a negative value
	// means no limit. 0 disable the pool.
	// +kubebuilder:default=-1
	MaxServers int `json:"maxServers,omitempty"`
	// +required
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Type=string
	// Schema used for BMC url with Refish
	RedfishSchema string `json:"redfishSchema"`
	// BrokerConfig is the name of the configmap containing the broker CA
	BrokerConfig string `json:"brokerConfig"`
	// Labels assigned to the baremetalhost created by the baremetalpool
	BmhLabels map[string]string `json:"bmhLabels,omitempty"`
	// Annotations assigned to the baremetalhost created by the baremetalpool
	BmhAnnotations map[string]string `json:"bmhAnnotations,omitempty"`
	// BlockingAnnotations contains a list of annotations which keeps the barematalhosts paused
	BlockingAnnotations []string `json:"blockingAnnotations,omitempty"`
	// Baremetalhost BootMode field
	// +kubebuilder:default="legacy"
	BootMode bmhv1alpha1.BootMode `json:"bootMode,omitempty"`
	// Baremetalhost online field
	// +kubebuilder:default=false
	Online bool `json:"online,omitempty"`
	// Baremetalhost Image field
	Image *bmhv1alpha1.Image `json:"image,omitempty"`
	// Baremetalhost UserData field
	UserData *corev1.SecretReference `json:"userData,omitempty"`
	// Baremetalhost MetaData field
	MetaData *corev1.SecretReference `json:"metaData,omitempty"`
	// Baremetalhost PreprovisioningNetworkDataName field
	PreprovisioningNetworkDataName string `json:"preprovisioningNetworkDataName,omitempty"`
	// Baremetalhost NetworkData field
	NetworkData *corev1.SecretReference `json:"networkData,omitempty"`
}

// BareMetalPoolStatus defines the observed state of BareMetalPool
type BareMetalPoolStatus struct {
	// List of the baremetalhost available
	BmhNamesList []string `json:"bmhnameslist,omitempty"`
	// Status of the pool
	Status string `json:"status"`
	// Number of baremetalhost available
	Replicas int32 `json:"replicas"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas
// +kubebuilder:printcolumn:name="Poolname",type="string",JSONPath=`.spec.poolName`
// +kubebuilder:printcolumn:name="Replicas",type="string",JSONPath=`.spec.replicas`
// +kubebuilder:printcolumn:name="Available",type="string",JSONPath=`.status.replicas`
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=`.status.status`
// BareMetalPool is the Schema for the baremetalpools API
type BareMetalPool struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BareMetalPoolSpec   `json:"spec,omitempty"`
	Status BareMetalPoolStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// BareMetalPoolList contains a list of BareMetalPool
type BareMetalPoolList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []BareMetalPool `json:"items"`
}

func init() {
	SchemeBuilder.Register(&BareMetalPool{}, &BareMetalPoolList{})
}
